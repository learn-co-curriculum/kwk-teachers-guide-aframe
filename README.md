## Objectives

1. Students will get an introduction to the A-Frame HTML library
2. Students will gain a deeper understanding of the interaction between JavaScript and HTML using guided lessons in coding for 3D

## Resources

* [A-Frame Introduction](https://aframe.io/docs/0.8.0/introduction/)
* [Building a Basic Scene in A-Frame](https://aframe.io/docs/0.8.0/guides/building-a-basic-scene.html)
* [Building an A-Frame Demo - MDN](https://developer.mozilla.org/en-US/docs/Games/Techniques/3D_on_the_web/Building_up_a_basic_demo_with_A-Frame)
* [A-Frame Examples](https://aframe.io/examples/)
